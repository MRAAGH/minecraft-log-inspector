# Minecraft log inspector

Maze's simple Minecraft log inspector

Displays an interactive graph for number of online players by date/hour.
Click anywhere in the graph to display nearest log entry.

![](https://mazie.rocks/files/log-inspector-screenshot.png)

### Dependencies

- Python 3.8 or later
- Matplotlib

### Usage

Download and unzip all your server logs and put them next to `loginspector.py`.
You can unzip all logs with this command:
```
gzip -d *.log.gz
```

Make sure the logs are named in this format:
`2021-06-20-1.log`

Exception is `latest.log` which is automatically recognized to be at the end.

Then run the script:
```
python3 loginspector.py
```

### License

This script is in the public domain. You can do whatever you want with it.
