#!/usr/bin/python3

# This file is in the public domain. You can do whatever you want.

"""

Maze's simple Minecraft log inspector

Displays an interactive graph for number of online players by date/hour.
Click anywhere in the graph to display nearest log entry.


Download and unzip all your server logs and put them next to this script.
You can unzip all logs with this command:
gzip -d *.log.gz

Make sure the logs are named in this format:
2021-06-20-1.log

An exception is latest.log which is considered to be at the end.

Then run the script:
python3 loginspector.py

"""


import glob
import datetime
import re
from matplotlib import pyplot, dates
import bisect

logs = glob.glob('*.log')

logs.sort()

timeregex = r'\[(..:..:..)\] .*'
joinleave = r'\[..:..:..\] \[Server thread/INFO]: (.*) (joined|left) the game'
serverstart = r'\[..:..:..\] \[Server thread/INFO]: Starting minecraft server version .*'

events = [] # (date, pname, "joined"/"left")

allevents = []
alleventdates = []

for fname in logs:
    # get date string from filename
    # (for latest.log, keep previous date string)
    if fname != 'latest.log':
        datestring = fname[:10]
    with open(fname, 'r') as f:
        for line in f:
            # check if time is specified
            if res1 := re.match(timeregex, line):
                # combine filename and time
                datedesc = datestring+'_'+res1.group(1)
                d = datetime.datetime.strptime(datedesc, '%Y-%m-%d_%H:%M:%S')
                allevents.append(line.strip())
                alleventdates.append(d)
                # check if it is a join/leave message
                if res2 := re.match(joinleave, line):
                    events.append((d, res2.group(1), res2.group(2)))
                # check if it is a "Starting minecraft server" message
                elif res2 := re.match(serverstart, line):
                    events.append((d, '[SERVER]', 'started'))

playernames = []
onlineplayers = []

# figure out which players were online at the start of the log
for e in events:
    pname = e[1]
    if pname == '[SERVER]':
        # stop searching because server was restarted here.
        break;
    # is it a player we have not encountered before?
    if not pname in playernames:
        playernames.append(pname)
        if e[2] == 'left':
            # player starts online
            # becasuse they leave before they join
            onlineplayers.append(pname)

del playernames

xs = []
ys = []

# make graph data for number of online players
for e in events:
    xs.append(e[0])
    ys.append(len(onlineplayers))
    pname = e[1]
    if pname == '[SERVER]':
        # everyone goes offline on server restart
        del onlineplayers[:]
    else:
        if e[2] == 'joined':
            # joined the game
            onlineplayers.append(pname)
        else:
            # left the game
            onlineplayers.remove(pname)
    xs.append(e[0])
    ys.append(len(onlineplayers))


# prepare click event callback
def callback(event):
    if event.xdata == None:
        return
    d = dates.num2date(event.xdata)
    d = d.replace(tzinfo=None)
    # find the first event to the right of the click
    i = bisect.bisect_left(alleventdates, d)
    # calculate gap on left and right of click
    gapright = alleventdates[i] - d
    gapleft = d - alleventdates[i-1]
    # check if left event or right event is closer
    if gapright < gapleft:
        print(allevents[i])
    else:
        print(allevents[i-1])
    return



pyplot.style.use('dark_background')
pyplot.grid(True, axis='x', color='#222222')
pyplot.plot(xs, ys)
pyplot.gca().xaxis.set_major_formatter(dates.DateFormatter('%b %d  %H:%M'))
pyplot.gcf().canvas.callbacks.connect('button_press_event', callback)
pyplot.ylabel('players online')
pyplot.show()




